package com.nostratech.project.controller;


import com.nostratech.project.persistence.service.MigrasiUserService2;
import com.nostratech.project.persistence.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/ldap1")
public class LDAPController2
{
    @Autowired
    MigrasiUserService2 ldapServicePRDSentul2;


    @RequestMapping
    (
            value = "/get-all-ldap-userOU-prd-sentul",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAllUserOUPRDSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul2.getAllUserAndAttributeAndObjectOUClassLDAP(true);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
            value = "/get-all-ldap-userOUParent-prd-sentul",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAllUserOUParentPRDSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul2.getAllUserAndAttributeAndObjectOUParentClassLDAP(true);
            }
        };
        return handler.getResult();
    }



    @RequestMapping
    (
            value = "/get-all-ldap-userOU-prd-ods",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAllUserOUPRDODS()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul2.getAllUserAndAttributeAndObjectOUClassLDAP(false);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
            value = "/get-all-ldap-userOUParent-prd-ods",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAllUserOUParentPRDODS()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul2.getAllUserAndAttributeAndObjectOUParentClassLDAP(false);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/insert-ldap-userOUParent-prd-sentul",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> insertLdapUserPRDSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul2.insertUserOUParentIntoODS();
            }
        };
        return handler.getResult();
    }
}