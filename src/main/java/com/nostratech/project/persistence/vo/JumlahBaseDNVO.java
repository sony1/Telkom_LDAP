package com.nostratech.project.persistence.vo;

import lombok.Data;

@Data
public class JumlahBaseDNVO
{
    private String baseDN7;
    private String baseDN8;
    private String baseDN9;
    private String baseDN10;
    private String baseDN11;
    private String baseDN12;
}