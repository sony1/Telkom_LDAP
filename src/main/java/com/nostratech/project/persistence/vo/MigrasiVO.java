package com.nostratech.project.persistence.vo;

import lombok.Data;

@Data
public class MigrasiVO
{
    private String strKey = "";
    private String strValue = "";
    private String baseDN = "";

    public MigrasiVO(String strKey, String strValue, String baseDN)
    {
        this.strKey += (strKey);
        this.strValue += (strValue);
        this.baseDN += (baseDN);
    }
}