package com.nostratech.project.persistence.vo;

import lombok.Data;

@Data
public class ObjectClassVO
{
    private String objClass;

    public ObjectClassVO(String objClass)
    {
        this.objClass = objClass;
    }
}