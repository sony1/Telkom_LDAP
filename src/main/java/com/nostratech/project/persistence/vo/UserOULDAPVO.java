package com.nostratech.project.persistence.vo;

import java.util.List;
import lombok.Data;

@Data
public class UserOULDAPVO {

    private String uid;
    private String givenName;
    private String mail;
    private String cn;
    private String sn;
    private String orclActiveStartDate;
    private String orclIsEnabled;
    private String orclSAMAccountName;
    private String pwdpolicysubentry;
    private String facebookID;
    private String myTelkomID;
    private String portalToken;
    private String twitterID;
    private String userPassword;
    private String ou;
    private List<ObjectClassVO> objectClassVO;
}
